FROM node:17 as builder
#Create workdir
WORKDIR app/
# Copy package.json and package-lock.json
COPY package*.json ./
# Install dependencies
RUN npm install
# Copy source code into Docker container
COPY . .
#build the app
RUN npm run build --prod

#Stage 2: Serve the application from Nginx
FROM nginx:1.21-alpine
#Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
#Copy nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf
# From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/dist/rx-angular /usr/share/nginx/html


