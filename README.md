# RxAngular - Change Detection preview

## About
This project represents an overview how the Angular's 'Change Detection' system works and how we can improve it using
@rx-angular/template.

#### Docs
https://www.rx-angular.io/docs/template

### License
MIT
