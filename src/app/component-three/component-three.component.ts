import {ChangeDetectionStrategy, Component, inject, ViewContainerRef} from '@angular/core';
import {TimeoutConfig} from "../timeout.config";

@Component({
  selector: 'app-component-three',
  templateUrl: './component-three.component.html',
  styleUrls: ['./component-three.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentThreeComponent {
  #vcr: ViewContainerRef = inject(ViewContainerRef);
  #timeout: number = inject(TimeoutConfig);

  logChangeDetection() {
    this.#vcr.element.nativeElement.style.backgroundColor = 'purple';

    setTimeout(() =>
        this.#vcr.element.nativeElement.style.backgroundColor = 'white',
      this.#timeout
    );

    console.log('Component Three Component rerender')

    return true;
  }
}
