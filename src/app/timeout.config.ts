import {InjectionToken} from "@angular/core";

export const TimeoutConfig = new InjectionToken<number>('TimeoutConfig')
