import {ChangeDetectionStrategy, Component, inject, Signal, ViewContainerRef} from '@angular/core';
import {DummyService} from "../dummy.service";
import {toSignal} from "@angular/core/rxjs-interop";
import {TimeoutConfig} from "../timeout.config";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageComponent {
  service = inject(DummyService);
  #vcr = inject(ViewContainerRef);
  #timeout = inject(TimeoutConfig);

  signalVal: Signal<number | undefined> = toSignal<number>(this.service.obs$);

  logChangeDetection() {
    this.#vcr.element.nativeElement.style.backgroundColor = 'green';

    setTimeout(() =>
        this.#vcr.element.nativeElement.style.backgroundColor = 'white',
      this.#timeout
    );

    console.log('Homepage Component rerender')

    return true;
  }

  onCounterChange(val: number) {
    console.log(val);
  }
}
