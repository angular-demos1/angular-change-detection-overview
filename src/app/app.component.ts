import {ChangeDetectionStrategy, Component, ElementRef, inject, Renderer2, Signal, viewChild} from '@angular/core';
import {TimeoutConfig} from "./timeout.config";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  rootDiv: Signal<ElementRef<HTMLDivElement> | undefined> = viewChild('rootDiv');
  #renderer = inject(Renderer2);
  #timeout = inject(TimeoutConfig);

  logChangeDetection() {
    const el: HTMLDivElement | undefined = this.rootDiv()?.nativeElement;

    this.#renderer.setStyle(el, 'backgroundColor', 'blue');

    setTimeout(() =>
        this.#renderer.setStyle(el, 'backgroundColor', 'white'),
      this.#timeout
    );

    console.log('App Root Component rerender');
    
    return true;
  }
}
