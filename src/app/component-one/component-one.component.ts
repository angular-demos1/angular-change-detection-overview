import {ChangeDetectionStrategy, Component, inject, ViewContainerRef} from '@angular/core';
import {TimeoutConfig} from "../timeout.config";

@Component({
  selector: 'app-component-one',
  templateUrl: './component-one.component.html',
  styleUrls: ['./component-one.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentOneComponent {
  #vcr = inject(ViewContainerRef);
  #timeout = inject(TimeoutConfig);

  logChangeDetection() {
    this.#vcr.element.nativeElement.style.backgroundColor = 'orange';

    setTimeout(() =>
        this.#vcr.element.nativeElement.style.backgroundColor = 'white',
      this.#timeout
    );

    console.log('Component One Component rerender')

    return true;
  }
}
