import {Injectable} from '@angular/core';
import {Observable, timer} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DummyService {
  obs$: Observable<number> = timer(0, 5000);
}
