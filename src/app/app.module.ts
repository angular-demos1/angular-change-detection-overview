import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomepageComponent} from './homepage/homepage.component';
import {RxLet} from "@rx-angular/template/let";
import {ComponentOneComponent} from './component-one/component-one.component';
import {ComponentTwoComponent} from './component-two/component-two.component';
import {ComponentThreeComponent} from './component-three/component-three.component';
import {CommonModule} from "@angular/common";
import {RxPush} from "@rx-angular/template/push";
import {TimeoutConfig} from "./timeout.config";
import {RxUnpatch} from "@rx-angular/template/unpatch";

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ComponentOneComponent,
    ComponentTwoComponent,
    ComponentThreeComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    RxLet,
    RxPush,
    RxUnpatch,
  ],
  providers: [{
    provide: TimeoutConfig,
    useValue: 1000
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
