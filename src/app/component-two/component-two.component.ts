import {ChangeDetectionStrategy, Component, inject, output, signal, ViewContainerRef} from '@angular/core';
import {DummyService} from "../dummy.service";
import {TimeoutConfig} from "../timeout.config";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-component-two',
  templateUrl: './component-two.component.html',
  styleUrls: ['./component-two.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentTwoComponent {
  service: DummyService = inject(DummyService);

  #vcr: ViewContainerRef = inject(ViewContainerRef);
  #timeout: number = inject(TimeoutConfig);

  /** ########################################### */

  /*
  as the old input
  @Input({required: true}) val: number | null | undefined;
  */

  /*
  as signal input
  val: InputSignal<number | null | undefined> = input.required<number | null | undefined>();
  */

  /** ########################################### */

  /*val: Signal<number | undefined> = toSignal<number>(this.service.obs$);*/

  /** ########################################### */

  /*
  @Output() counterChange = new EventEmitter<number>();
   */

  counterChange = output<number>()
  /** ########################################### */

  _counter = 0;
  counter = signal(0);
  counter$ = new BehaviorSubject<number>(0);

  /** ########################################### */

  constructor() {
    /*
    needed for the output

    timer(0, 2000).pipe(takeUntilDestroyed()).subscribe(() =>
      this.counterChange.emit(this._counter)
    );
     */
  }

  logChangeDetection() {
    console.log('Component Two Component rerender');

    this.#vcr.element.nativeElement.style.backgroundColor = 'red';

    setTimeout(() =>
        this.#vcr.element.nativeElement.style.backgroundColor = 'white',
      this.#timeout
    );

    return true;
  }

  increment() {
    this._counter++;
    //this.counter.set(this.counter() + 1);
    //this.counter$.next(this.counter$.value + 1);
  }
}
